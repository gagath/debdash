#!/usr/bin/env python3

import re

import psycopg2
import graphviz
import requests


UDD_HOST = "udd-mirror.debian.net"
UDD_USER = "udd-mirror"
UDD_PW = "udd-mirror"
UDD_DB = "udd"

COLOR_DONE = "lightgreen"
COLOR_NEW = "lightsteelblue1"
COLOR_TBD = "lightyellow"
COLOR_WAITING = "lightsalmon"


def status_to_color(status, root, is_new):
    """Choose a color depending on the status, root"""
    if status == "done":
        return COLOR_DONE
    elif is_new:
        return COLOR_NEW
    elif root:
        return COLOR_TBD    
    return COLOR_WAITING


def gen_legend():
    """Generate the legend subgraph."""

    legend = graphviz.Digraph(
        name="cluster_legend",
        graph_attr={"rankdir": "TB", "label": "Legend"},
        edge_attr={"style": "invis"},
    )
    legend.node("tbd", label="TBD", fillcolor=COLOR_TBD)
    legend.node("waiting", label="Waiting", fillcolor=COLOR_WAITING)
    legend.node("new", label="NEW", fillcolor=COLOR_NEW)
    legend.node("done", label="Done", fillcolor=COLOR_DONE)
    legend.edge("tbd", "waiting")
    legend.edge("waiting", "new")
    legend.edge("new", "done")

    return legend


def get_packages_in_new():
    r = requests.get("https://ftp-master.debian.org/new.822")
    assert r.status_code == 200

    s = re.compile(r"Source: (.*)")
    lines = r.text.splitlines()

    sources = [m.group(1) for l in lines if (m := s.match(l)) is not None]
    return sources


with psycopg2.connect(f"host={UDD_HOST} user={UDD_USER} password={UDD_PW} dbname={UDD_DB}") as conn:
    conn.set_client_encoding("utf-8")
    with conn.cursor() as cur:
        cur.execute(
            """
WITH RECURSIVE cte as (
  SELECT
    bugs.id,
    bugs.title,
    bugs.package,
    bugs.status,
    bugs_blockedby.blocker as blocker,
    false as archived,
    true as root
  FROM
    bugs
    LEFT JOIN bugs_blockedby ON bugs.id = bugs_blockedby.id
  WHERE
    bugs.package = 'wnpp'
    AND bugs.owner_email = 'debian@microjoe.org'
    AND bugs.status != 'done'

  UNION ALL
  SELECT
    b.id,
    b.title,
    b.package,
    b.status,
    bugs_blockedby.blocker as blocker,
    b.archived,
    b.root
  FROM
    (
      SELECT
        *, false as archived, false as root
      FROM
        bugs
      UNION ALL
      SELECT
        *, true as archived, false as root
      FROM
        archived_bugs
    ) b
  JOIN cte ON b.id = cte.blocker
  LEFT JOIN bugs_blockedby ON b.id = bugs_blockedby.id

)
SELECT
  DISTINCT *
FROM
  cte
        """
        )

        # breakpoint()

        dot = graphviz.Digraph(
            comment="My Dashboard",
            graph_attr={"fontname": "sans", "fontsize": "10pt"},
            node_attr={
                "shape": "box",
                "style": "filled",
                "fontname": "sans",
                "fontsize": "10pt",
                "width": "0.1",
                "height": "0.1",
            },
            edge_attr={"fontname": "sans", "fontsize": "10pt"},
        )

        email = "debian@microjoe.org"

        dot.subgraph(gen_legend())

        packages = graphviz.Digraph(
            name="packages", graph_attr={"label": f"Ongoing work for {email}"}
        )

        new_packages = get_packages_in_new()

        for bug, bug_name, package, status, dep, archived, root in cur:

            abbr_name = f"{package}: {bug_name}"

            in_new = False
            # Directly show wnpp requests (ITP, ITA…)
            if package == "wnpp":
                abbr_bug_name = bug_name.split(" -- ")[0]
                abbr_name = f"{abbr_bug_name}"
                itp_name = re.match(r"ITP: (.*)", abbr_name)
                if itp_name:
                    if itp_name.group(1) in new_packages:
                        in_new = True
            # Directly show RFS requests
            elif package == "sponsorship-requests":
                abbr_name = bug_name.split("/")[0]
            # Only show source package + bin package in other bugs
            else:
                abbr_name = bug_name.split(":")[0] + " […]"

            color = status_to_color(status, root, in_new)

            packages.node(
                str(bug),
                abbr_name,
                tooltip=f"#{bug}: {package}: {bug_name}",
                URL=f"https://bugs.debian.org/cgi-bin/bugreport.cgi?bug={bug}",
                fillcolor=color,
            )

            if dep:
                packages.edge(str(bug), str(dep))

        dot.subgraph(packages)
        print(dot.source)
