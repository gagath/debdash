#!/usr/bin/env python3

import re

import psycopg2


UDD_HOST = "udd-mirror.debian.net"
UDD_USER = "udd-mirror"
UDD_PW = "udd-mirror"
UDD_DB = "udd"


with psycopg2.connect(f"host={UDD_HOST} user={UDD_USER} password={UDD_PW} dbname={UDD_DB}") as conn:
    conn.set_client_encoding("utf-8")
    with conn.cursor() as cur:

        # from https://salsa.debian.org/qa/udd/blob/master/web/sponsorstats.cgi
        cur.execute(
            """
SELECT s.source,
       u.version,
       u.changed_by,
       nmu,
       signed_by,
       cl.login
FROM sources s,
     upload_history u,
     carnivore_emails ce1,
     carnivore_emails ce2,
     carnivore_login cl
WHERE s.distribution = 'debian'
  AND s.release = 'sid'
  AND s.source = u.source
  AND u.changed_by_email = ce1.email
  AND u.signed_by_email = ce2.email
  AND ce1.id != ce2.id
  AND ce2.id = cl.id
  AND u.changed_by_email LIKE 'debian@microjoe.org'
ORDER BY s.source,
         u.version
"""
        )

        dct = {}
        for source, version, changed_by, nmu, signed_by, login in cur:
          if login in dct:
            dct[login].append((source, version))
          else:
            dct[login] = [(source, version)]
          
        for login, reviews in dct.items():
            print(login, len(reviews))
            for source, version in reviews:
              print(f"    {source} {version}")
