import sqlalchemy as sa
from sqlalchemy import Table, Column, Integer, String
from sqlalchemy.sql.schema import ForeignKey

import attr
from sqlalchemy.orm import registry, relationship

mapper_registry = registry()


@mapper_registry.mapped
@attr.s
class BlockedBy:
    __table__ = Table(
        "bugs_blockedby",
        mapper_registry.metadata,
        Column("id", ForeignKey("bugs.id"), primary_key=True),
        Column("blocker", ForeignKey("bugs.id")),
    )
    id = attr.ib()
    blocker = attr.ib()


@mapper_registry.mapped
@attr.s
class Bug:
    __table__ = Table(
        "bugs",
        mapper_registry.metadata,
        Column("id", Integer, primary_key=True),
        Column("package", String),
        Column("title", String),
        Column("owner_email", String),
    )
    id = attr.ib()
    package = attr.ib()
    title = attr.ib()
    owner_email = attr.ib()

    blockers = relationship("BlockedBy", foreign_keys=BlockedBy.id)


if __name__ == "__main__":
    host = "udd-mirror.debian.net"
    user = "udd-mirror"
    psw = "udd-mirror"
    db = "udd"

    # create an engine
    pengine = sa.create_engine(
        "postgresql+psycopg2://" + user + ":" + psw + "@" + host + "/" + db, future=True
    )

    s = sa.orm.sessionmaker(pengine, future=True)()
    bugs = (
        s.query(Bug)
        .filter(Bug.package == "wnpp", Bug.owner_email == "debian@microjoe.org")
        .join(Bug.blockers)
    )

    # print(bugs)

    for b in bugs.all():
        print(b.title)
        if b.blockers:
            for bl in b.blockers:
                print(bl.blocker)
                blo = s.query(Bug).where(Bug.id == bl.blocker).first()
                if blo:
                    print(f"    blocked by {blo.package}: {blo.title}")
