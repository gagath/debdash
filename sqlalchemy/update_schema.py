from sqlalchemy.ext.automap import automap_base
from sqlalchemy import create_engine

import pickle

host = "udd-mirror.debian.net"
user = "udd-mirror"
psw = "udd-mirror"
db = "udd"

Base = automap_base()

# create an engine
pengine = create_engine(
    "postgresql+psycopg2://" + user + ":" + psw + "@" + host + "/" + db, future=True
)

Base.prepare(pengine, reflect=True)

with open("metadata.pickle", "wb") as cache_file:
    cached_metadata = pickle.dump(Base.metadata, file=cache_file)

with open("classes.pickle", "wb") as cache_file:
    cached_metadata = pickle.dump(Base.classes, file=cache_file)
