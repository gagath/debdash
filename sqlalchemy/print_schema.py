import pickle

import sqlalchemy as sa
from sqlalchemy.ext.automap import automap_base
from sqlalchemy.orm import Session, relationship
from sqlalchemy import create_engine

host = "udd-mirror.debian.net"
user = "udd-mirror"
psw = "udd-mirror"
db = "udd"

# create an engine
pengine = create_engine(
    "postgresql+psycopg2://" + user + ":" + psw + "@" + host + "/" + db,
    future=True,
    client_encoding="utf-8",
)

with open("metadata.pickle", "rb") as cache_file:
    cached_metadata = pickle.load(file=cache_file)

cached_metadata.bind = pengine
# cached_metadata.reflect()

# Base = declarative_base(bind=pengine, metadata=cached_metadata)
Base = automap_base(bind=pengine, metadata=cached_metadata)
Base.prepare()

Bug = Base.classes.bugs
BugBlockedBy = Base.classes.bugs_blockedby

if __name__ == "__main__":
    s = Session(pengine, future=True)
    bugs = s.query(Bug).where(
        Bug.package == "wnpp", Bug.owner_email == "debian@microjoe.org"
    )

    import pdb

    pdb.set_trace()

    # print(bugs.id, bugs.title)
    print("digraph packages {")
    for b in bugs.all():
        blockers = (
            s.query(BugBlockedBy)
            .join(Bug, Bug.id == BugBlockedBy.blocker)
            .where(BugBlockedBy.id == b.id)
        )

        for dep in blockers.all():
            me = f"{b.id}: {b.title}"
            blocker = f"{dep.bugs.id}: {dep.bugs.title}"
            # import pdb; pdb.set_trace()
            print(f"    {me} -> {blocker};")

        import pdb

        pdb.set_trace()

    print("}")
